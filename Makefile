# ==============================================================================
# Makefile for local project.
# This works on Windows, but you will need cygwin and you will need to add
# R, Rscript and make to the user's path. It should work out of the box on
# OS X, if you have make installed.
# ==============================================================================


# Update Data -----------------------------------------------------------------
# Downloads PPR data from Open Data New York (see scripts for details) and
# creates data objects which are used in the vignette analyses.
#
update_data: data-raw/ppr_all.R data-raw/ppr_medicaid.R
	Rscript 'data-raw/ppr_medicaid.R'
	Rscript 'data-raw/ppr_all.R'

# Update Documentation ---------------------------------------------------------
# Builds the various documentation pieces of the package.
#
update_documentation: data/README.Rmd
	Rscript -e 'rmarkdown::render("data/README.Rmd")'
	Rscript -e 'devtools::document()'

# Vignettes --------------------------------------------------------------------
# Compiles the analyses in vignettes/. This makes is convenient to update
# al the vignettes. As needed, this can also be broken down to be more modular.
#
vignettes: vignettes/EDA.Rmd vignettes/PPR-All.Rmd vignettes/PPR-Medicaid.Rmd
	Rscript -e 'devtools::clean_vignettes()'
	Rscript -e 'devtools::build_vignettes()'

# Clean ------------------------------------------------------------------------
# Cleans up to remnants from compiling vignettes.
#
clean:
	Rscript -e 'devtools::clean_vignettes()'
