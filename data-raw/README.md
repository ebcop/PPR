# README - data-raw

# Data Objects

Analysis based on two data objects.

1. PPR All Payers (ppr_all)
2. PPR Medicaid (ppr_medicaid)

To update the data objects, run:

    make -b update_data
    
Both objects are stored a rda files in data/.

# PPR All Payers

- Hospital Inpatient Potentially Preventable Readmission (PPR) Rates
  by Hospital (SPARCS): Beginning 2009
- The dataset contains Potentially Preventable Readmission observed,
  expected, and risk adjusted rates by hospital for all payer
  beneficaries beginning in 2009. 
- https://health.data.ny.gov/Health/Hospital-Inpatient-Potentially-Preventable-Readmis/amqp-cz9w
- Data object is built by: "ppr_all.R"

# PPR Medicaid

- Medicaid Hospital Inpatient Potentially Preventable Readmission
  (PPR) Rates by Hospital: Beginning 2011
- The dataset contains Potentially Preventable Readmission observed,
  expected, and risk adjusted rates by hospital for Medicaid enrollees
  beginning in 2011.
- https://health.data.ny.gov/Health/Medicaid-Hospital-Inpatient-Potentially-Preventabl/ckvf-rbyn
- Data object is built by: "ppr_medicaid.R"
