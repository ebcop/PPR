# Goals

The PPR Demo project has a number of goals, as listed below:

- Provide a demo project / analysis in R using Open Data.
- Provide a demo project / analysis with which to showcase the use of
  Git and GitLab for open science.
  - This includes tracking changes.
  - Connecting Issues with Submissions / Pull Requests.
- Assess two hypotheses formed by the authors (see below).

For demonstrative purposes, this demo repo is based around the R
package structure, with a few small deviations. That said, it is not
intended to ever be uploaded to CRAN or "installed" in the traditional
manner. This project uses the package structure in order to take
advantage of devtools' useful features.

R's working directory should be the project root folder. Most devtools
commands work as expected and are used in vignettes/, data-raw/, etc.

# Hypotheses

The putative goal of this repo is to assess two hypotheses:

- Hypothesis 1: The PPR Rate is, on average, decreasing in New York
  State for both data sets we have access to on Open Data New York.
  1. All Payers
  2. Medicaid Enrollees
- Hypothesis 2: We can replicate Hypothesis 1 analysis (for Medicaid
  Enrollees) using Clinical Data Mart (CDM) data and reach the same
  basic conclusion.

The data objects used in this analysis are documented in the
data/README.md file.

# Setup

To look at this project, you need a web-browser (otherwise . . . how
did you find these words). To download the analysis to your local
computer you can either hit the download button . . . . OR . . . . run
the following git command:

    git clone https://gitlab.com/ebcop/PPR.git 
    
This command will download the code to your local PC, in a folder
called PPR. If you would like to submit changes to this project,
please create a fork of the project and submit pull requests to:

andy.choens@health.ny.gov OR kabanga.mbuyi@health.ny.gov

Please be aware that this project is intended to be a teaching
environment, not a formal research project per se.

## Dependencies

The vignettes presented in this repo require several packages from
CRAN to run. The following R code snippet will install ALL packages
needed to compile the vignettes.

    packages <- c("devtools","dplyr","ggplot2","knitr","pander","rmarkdown","readr")
    install.packages( packages, dependencies = TRUE )

Calls to load_all() will fail if any of these packages are missing.

Building the data files requires additional dependencies. If you would
like to update the data files provided here, you must install x
additional packages. Again, these are ONLY needed if you plan to run
update\__data in the Makefile. These packages are listed as suggested
in the DESCRIPTION file.

    packages <- c("RODBC","RODBCext","roxygen2","stringi","testthat")
    install.packages( packages, dependencies = TRUE )

Calls to load_all() will work if these packages are missing, but you
will not be able to run the code in data-raw/ or tests/.

## Vignettes

The code in these vignettes assumes the R working directory is the
root PPR Project folder (as per the Makefile). If you run R from the
vignettes/ folder, the code won't work.

The current repo contains three analyses:

1. EDA: Exploratory Data Analysis of the two data sets. Explains some
   fo the decisions made in how this repo is oranized.
2. PPR-All: Analysis of the PPR Rates data for 2009 - 2014 All Payers.
3. PPR-MEdicaid: Analysis of the PPR RAte data for 2011 - 2013 for
   both All Payers and Medicaid Enrollees. Compares the two groups.
   
Rmarkdown files are stored in vignettes/. Compile reports, processed
code, etc. is store in inst/doc/.

## Make

Make is a simple build utility. It is part of traditional build system
of any Unix system. In this case, we are using GNU Make.

To update the data: 

    make -b update_data
    
To update the vignettes:

    make -b vignettes
