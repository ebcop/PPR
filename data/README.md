    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    ## Loading PPR

Data Objects
============

Analysis based on two data objects.

1.  PPR All Payers (ppr\_all)
2.  PPR Medicaid (ppr\_medicaid)

Both objects are cleaned after getting downloaded.

-   Replaces spaces in column names with underscores.
-   All column names are lower case.
-   Removes rows where discharge\_year or facility name is null.
-   Removes rows where facility\_name is "Statewide". This can be
    derived and isn't necessary.
-   For ppr\_medicaid only, removes the Duals and Not Duals rows. This
    information exists only for 2013 and has not comparable data in the
    All Payer data, so it is removed. When there are three or more years
    of Dual / Not Dual data, this decision can be re-visited.
-   Due to requests, repo now includes both data objects as CSV files,
    in the data/csv/ folder. Apparently, someone wants to play with SAS.

PPR All Payers
==============

-   Hospital Inpatient Potentially Preventable Readmission (PPR) Rates
    by Hospital (SPARCS): Beginning 2009
-   The dataset contains Potentially Preventable Readmission observed,
    expected, and risk adjusted rates by hospital for all payer
    beneficaries beginning in 2009.
-   <https://health.data.ny.gov/Health/Hospital-Inpatient-Potentially-Preventable-Readmis/amqp-cz9w>

<!-- -->

    ## Observations: 1,321
    ## Variables: 9
    ## $ discharge_year         (int) 2014, 2014, 2014, 2014, 2014, 2014, 201...
    ## $ facility_id            (int) 1305, 181, 812, 192, 9250, 207, 804, 79...
    ## $ facility_name          (chr) "Maimonides Medical Center", "Vassar Br...
    ## $ hospital_county        (chr) "Kings", "Dutchess", "Saint Lawrence", ...
    ## $ at_risk_admissions     (int) 35816, 16496, 300, 3875, 2213, 16967, 1...
    ## $ observed_ppr_chains    (int) 1630, 1094, 30, 151, 12, 1381, 130, 194...
    ## $ observed_ppr_rate      (dbl) 4.55, 6.63, 10.00, 3.90, 0.54, 8.14, 7....
    ## $ expected_ppr_rate      (dbl) 4.98, 6.28, 8.13, 4.36, 0.80, 8.72, 6.5...
    ## $ risk_adjusted_ppr_rate (dbl) 5.82, 6.73, 7.84, 5.69, 4.33, 5.94, 6.9...

    ## [1] 1321    9

To load the data in an active R session:

    library(devtools)
    load_all()
    data(ppr_all)

PPR Medicaid
============

-   Medicaid Hospital Inpatient Potentially Preventable
    Readmission (PPR) Rates by Hospital: Beginning 2011
-   The dataset contains Potentially Preventable Readmission observed,
    expected, and risk adjusted rates by hospital for Medicaid enrollees
    beginning in 2011.
-   <https://health.data.ny.gov/Health/Medicaid-Hospital-Inpatient-Potentially-Preventabl/ckvf-rbyn>

<!-- -->

    ## Observations: 567
    ## Variables: 7
    ## $ discharge_year         (int) 2013, 2012, 2011, 2013, 2012, 2011, 201...
    ## $ facility_name          (chr) "ADIRONDACK MEDICAL CENTER", "ADIRONDAC...
    ## $ at_risk_admissions     (int) 526, 540, 531, 7830, 7159, 6604, 7, 7, ...
    ## $ observed_ppr_chains    (int) 33, 30, 20, 492, 451, 458, 0, 0, 0, 35,...
    ## $ observed_ppr_rate      (dbl) 6.27, 5.56, 3.77, 6.28, 6.30, 6.94, 0.0...
    ## $ expected_ppr_rate      (dbl) 6.52, 6.81, 7.37, 6.41, 6.52, 7.09, 6.0...
    ## $ risk_adjusted_ppr_rate (dbl) 6.05, 4.87, 3.46, 6.16, 5.77, 6.62, 0.0...

    ## [1] 567   7

To load the data in an active R session:

    library(devtools)
    load_all()
    data(ppr_medicaid)
